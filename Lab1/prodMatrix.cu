#include <stdio.h>
#include <stdlib.h>

__host__ void printMatrix (int* matrix, int rows, int cols)
{
    int idx = 0;
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            idx = i * cols + j;
            printf("%i ", matrix[idx]);
        }
        printf("\n");
    } 

}

__global__ void fillMatrix (int* matrix)
{
    int idx = threadIdx.x;
    matrix[idx] = idx % 10;
}

__global__ void prodMatrix (int* A, int* B, int* R, int dim1, int dim2)
{
   int idx = threadIdx.x;
   
   R[idx] = 0;
        
   for(int i = 0; i < dim2; i++)
       R[idx] += A[(idx/dim1)*dim2 + i] * B[idx%dim1 + dim1*i];
}

int main()
{
    int dim1 = 2, dim2 = 3;
    int size_F = dim1 * dim2 * sizeof(int); // size of factor
    int size_R = dim1 * dim1 * sizeof(int); // size of result

    // allocate matrices on host
    int *mat_A, *mat_B, *mat_R;
    mat_A = (int*) malloc(size_F);
    mat_B = (int*) malloc(size_F);
    mat_R = (int*) malloc(size_R);

    // allocate matrices on device
    int *d_mat_A, *d_mat_B, *d_mat_R;
    cudaMalloc(&d_mat_A, size_F);
    cudaMalloc(&d_mat_B, size_F);
    cudaMalloc(&d_mat_R, size_R);
    
    // fill matrices from device
    fillMatrix <<<1, dim1*dim2>>> (d_mat_A);
    fillMatrix <<<1, dim1*dim2>>> (d_mat_B);
    
    // calculate product
    prodMatrix <<<1, dim1*dim1>>> (d_mat_A, d_mat_B, d_mat_R, dim1, dim2);

    // copy from device to host
    cudaMemcpy(mat_A, d_mat_A, size_F, cudaMemcpyDeviceToHost);
    cudaMemcpy(mat_B, d_mat_B, size_F, cudaMemcpyDeviceToHost); 
    cudaMemcpy(mat_R, d_mat_R, size_R, cudaMemcpyDeviceToHost); 

    // print stuff
    printMatrix(mat_A, dim1, dim2);
    printf("---------------------\n");
    printMatrix(mat_B, dim2, dim1); 
    printf("---------------------\n");
    printMatrix(mat_R, dim1, dim1);

    // free stuff
    free(mat_A);
    free(mat_B);
    free(mat_R);
    
    cudaFree(d_mat_A);
    cudaFree(d_mat_B);
    cudaFree(d_mat_R);
    
    cudaDeviceReset();
    
    return 0;
}

