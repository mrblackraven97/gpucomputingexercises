#include <stdio.h>
#include <stdlib.h>
#include "common.h"

__host__ void fillRandom(int* A, int dim)
{
    for(int i = 0; i < dim; i++)
        A[i] = rand() % 10;
}

__host__ void printArray(int* A, int dim)
{
    for(int i = 0; i < dim; i++)
        printf("%i ", A[i]);
    printf("\n");
}

__global__ void sumByN(int n, int* A, int dimA, int* R)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    R[idx] = 0;

    for (int i = 0; i < n && idx*n + i < dimA; i++)
        R[idx] += A[idx*n + i];
}

int main()
{
    // allocate aux vars
    int dim = 10;
    int size = dim * sizeof(int);
    int n = 10;
    int resultDim = dim/n + (dim%n != 0);
    int resultSize = resultDim * sizeof(int);

    // allocate cpu vars
    int *array, *result;
    array = (int*) malloc(size);
    result = (int*) malloc(resultSize);

    // allocare gpu vars
    int *d_array, *d_result;
    cudaMalloc(&d_array, size);
    cudaMalloc(&d_result, resultSize);

    // fill array
    fillRandom(array, dim);
    
    // copy from cpu to gpu array
    cudaMemcpy(d_array, array, size, cudaMemcpyHostToDevice);

    sumByN <<<1, resultDim>>>(n, d_array, dim, d_result);

    // copy from dev to host
    cudaMemcpy(result, d_result, resultSize, cudaMemcpyDeviceToHost);

    // print array 
    printArray(array, dim);
    printArray(result, resultDim);

}
