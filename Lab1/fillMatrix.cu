#include <stdlib.h>
#include <stdio.h>
#include "common.h"

__host__ void printMatrix (int* matrix, int rows, int cols)
{
    int idx = 0;
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            idx = i * cols + j;
            printf("%i ", matrix[idx]);
        }
        printf("\n");
    } 

}

__global__ void fillMatrix (int* matrix)
{
    printf("i'm in gpu, row: %i, col: %i \n", blockIdx.x, threadIdx.x);
    
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    printf("%i\n", idx);

    matrix[idx] = idx;
   
    // equivalente
    //int* ptr = matrix + idx;
    //*ptr = idx;

}

int main(void) 
{
    // allocate stupid variables
    int rows = 2, cols = 5, idx = 0;
    int mallocSize = sizeof(int) * rows * cols;

    // allocate matrix on host
    int* m; 
    m = (int*) malloc(mallocSize);
    
    // allocate matrix on device
    int* d_m;
    cudaMalloc(&d_m, mallocSize);

    // set host values
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            idx = i * cols + j;
            m[idx] = 0;
        }
    }
    
    printMatrix(m, rows, cols);

    // copy data from host to device
    CHECK(cudaMemcpy(d_m, m, mallocSize, cudaMemcpyHostToDevice));

    // do stuff on device
    fillMatrix <<<2,5>>>(d_m);

    cudaDeviceSynchronize();

    // copy data from device to host
    cudaMemcpy(m, d_m, mallocSize, cudaMemcpyDeviceToHost);

    printMatrix(m, rows, cols);
    
    // free host matrix
    free(m);

    // free device matrix
    cudaFree(d_m);
    
    cudaDeviceReset();

    return 0;
}
