/**
 *
 * This program uses the host CURAND API to generate 100
 * pseudorandom floats.
 */
#include <stdio.h>
#include <stdlib.h>
#include <curand.h>

#define CUDA_CALL(x) do { if((x)!=cudaSuccess) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)
#define CURAND_CALL(x) do { if((x)!=CURAND_STATUS_SUCCESS) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)

__global__ void calculateStats(float* rollData, int* resData)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int min = 6;
    int roll = 0;

    resData[idx] = 0;

    // aggregation
    for(int i = 0; i < 4; i++)
    {
       roll = ((int)(rollData[idx*4+i] * 100) % 6) + 1;
       
       resData[idx] += roll;
       
       printf("th:%i roll:%i \n", idx, roll);

       if(roll < min)
          min = roll;
       
    }

    resData[idx] -= min;
}

int main(int argc, char *argv[]) {
	size_t n = 24;
	size_t i;
    size_t resSize = n/4;
	curandGenerator_t gen;
	float *devData, *hostData;
    int *resData, *statData;

	/* Allocate n floats on host */
	hostData = (float *) calloc(n, sizeof(float));
    statData = (int *) calloc(resSize, sizeof(int));

	/* Allocate n floats on device */
	CUDA_CALL(cudaMalloc((void ** )&devData, n * sizeof(float)));
    CUDA_CALL(cudaMalloc((void ** )&resData, resSize * sizeof(int)));

	/* Create pseudo-random number generator */
	CURAND_CALL(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));

	/* Set seed */
	CURAND_CALL(curandSetPseudoRandomGeneratorSeed(gen, time(NULL)));
    
	/* Generate n floats on device */
	CURAND_CALL(curandGenerateUniform(gen, devData, n));

    /* Use a kernel to edit data */
    calculateStats <<<1, resSize>>> (devData, resData); 

	/* Copy device memory to host */
	CUDA_CALL(
			cudaMemcpy(hostData, devData, n * sizeof(float),
					cudaMemcpyDeviceToHost));
	CUDA_CALL(
			cudaMemcpy(statData, resData, resSize * sizeof(float),
					cudaMemcpyDeviceToHost));

	/* Show result */
	for (i = 0; i < n; i++) {
		printf("%1.4f ", hostData[i]);
	}
	printf("\n");

	for (i = 0; i < resSize; i++) {
		printf("%i ", statData[i]);
	}
    printf("\n");

    /* Cleanup */
	CURAND_CALL(curandDestroyGenerator(gen));
	CUDA_CALL(cudaFree(devData));
	free(hostData);
	return EXIT_SUCCESS;
}
